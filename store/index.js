import Vuex from 'vuex'

import { state } from './state'
import { mutations } from './mutations'
import { actions } from './actions'
import { getters } from './getters'

import scrollMenu from './modules/scrollMenu'
import form from './modules/form'
import user from './modules/user'
import content from './modules/content'

const createStore = () => {
  return new Vuex.Store({
    modules: { scrollMenu, form, user, content },
    state,
    mutations,
    actions,
    getters
  })
}

export default createStore
