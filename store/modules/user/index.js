const state = () => ({
	user: null
})

const mutations = {}

const actions = {}

const getters = {
	isUserAuthenticated (state) {
		return state.user && !(Object.keys(state.user).length === 0 && obj.constructor === Object)
	}
}

export default { namespaced: true, state, mutations, actions, getters }
