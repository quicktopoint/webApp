export const state = () => ({
	menu: [
		{ id: '0', name: 'Jak to działa', url: '#jak-to-dziala' },
		{ id: '1', name: 'Integracje', url: '#integracje' },
		{ id: '2', name: 'Pobierz aplikację', url: '#pobierz-aplikacje' },
		{ id: '3', name: 'Zostań kierowcą', url: '#zostan-kierowca' }
	],
	activeMenuItem: null,
	showMobileNav: false
})

const mutations = {
	setActiveMenuItem (state, payload) {
		state.activeMenuItem = payload
	}
}

const actions = {
	changeActiveMenuItem ({ commit }, payload) {
		commit('setActiveMenuItem', payload)
	}
}

const getters = {
	menu (state) {
		return state.menu
	},
	activeMenuItem (state) {
		return state.activeMenuItem
	}
}

export default { namespaced: true, state, mutations, actions, getters }
