export const state = () => ({
	head: {
		faq: {
			title: 'SimpliCity Connect - FAQ',
			description: 'My custom description'
		},
		contact: {
			title: 'SimpliCity Connect - Kontakt',
			description: 'My custom description'
		},
		company: {
			title: 'SimpliCity Connect - O firmie',
			description: 'My custom description'
		},
		regulations: {
			title: 'SimpliCity Connect - Regulamin',
			description: 'My custom description'
		},
		rememberPass: {
			title: 'SimpliCity Connect - Przypomnij hasło',
			description: 'My custom description'
		}
	},

	companyName: 'SimpliCity Connect',
	headlineH1: 'Dostarcz przesyłkę <br> w mniej niż dobę',
	ourSuppliersTitle: 'Nasi dostawcy:',
	ourSuppliers: [
		{ id: '0', name: 'Piesi', icon: 'onfoot-with-circle', color: '#00eb8a' },
		{ id: '1', name: 'Rowerzyści', icon: 'bike-with-circle', color: '#80b4ff' },
		{ id: '2', name: 'Samochód', icon: 'car-with-circle', color: '#ffaa30' },
		{ id: '3', name: 'Busy', icon: 'bus-with-circle', color: '#d780ff' },
		{ id: '4', name: 'Ciężarówki', icon: 'truck-with-circle', color: '#ff2121' }
	],
	loginBtn: 'Logowanie',
	registerBtn: 'Rejestracja',
	toggleMenu: 'Otwórz lub zamknij menu',
	closeMenu: 'Zamknij menu',
	read: 'Czytaj',
	download: 'Pobierz',
	downloadDocumentation: 'Pobierz dokumentację',

	form: {
		error: {
			email: 'Wpisz poprawy adres email.',
			required: 'To pole jest wymagane',
			numeric: 'To pole przyjmuje tylko liczby',
			passwordMinLeng: 'Hasło musi składać się z minimum 6 znaków',
			notTheSamePassword: 'Podane hasła różnią się'
		},
		label: {
			name: 'Twoje imię',
			firstAndLastName: 'Imię i nazwisko',
			email: 'Adres e-mail',
			password: 'Hasło',
			confirmPassword: 'Powtórz hasło',
			phone: 'Telefon',
			city: 'Miasto',
			chooseTransportType: 'Wybierz preferowane środki transportu:'
		},
		submit: {
			send: 'Wyślij',
			loginToSend: 'Zaloguj się by wysłać'
		}
	},

	login: {
		orText: 'lub wypełnij formularz logowania',
		submit: 'Zaloguj się',
		register: 'Załóż konto',
		loginByFacebook: 'Zaloguj się przy pomocy Faebooka',
		loginByGoogle: 'Zaloguj się przy pomocy Google',
		didYouForgetPassword: 'Zapomniałeś hasło?',
		doYouHaveAcount: 'Nie posiadasz jeszcze konta?'
	},

	register: {
		orText: 'lub wypełnij formularz rejestracji',
		termsNote: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
		submit: 'Utwórz konto',
		login: 'Zaloguj się',
		loginByFacebook: 'Zaloguj się przy pomocy Faebooka',
		loginByGoogle: 'Zaloguj się przy pomocy Google',
		yourShippingIsReady: 'Twoja przesyłka jest gotowa do nadania',
		doYouHaveAcount: 'Posiadasz już konto?',
		accountBenefitsTitle: 'Załóż konto aby:',
		accountBenefitsList: [ 'Lorem ipsum', 'Ipsum lorem', 'Dolor sit amet' ]
	},

	downloadApp: {
		headline: 'Pobierz aplikację',
		storesList: [
			{ id: '0', name: 'App Store', href: '#', icon: 'app-store' },
			{ id: '1', name: 'Google Play', href: '#', icon: 'google-play' }
		],
		title: 'Aplikacja <br> mobilna',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi. Ut blandit vestibulum orci, nec maximus felis. Etiam finibus neque in purus consequat tincidunt.'
	},

	howItWorks: {
		title: 'SimpliCity Connect to nowa jakość dostaw',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi. Ut blandit vestibulum orci, nec maximus felis. Etiam finibus neque in purus consequat tincidunt. Donec semper sit amet ante eget varius. Cras pulvinar in nisl sit amet pulvinar. Suspendisse sit amet lacinia justo',
		ctaButton: 'Zamów dostawę',
		panelTitle: 'Jak to działa?',
		panelDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis.',
		panelMonitor: 'Nadajesz przesyłkę online lub za pomocą aplikacji mobilnej',
		panelCar: 'Nadajesz przesyłkę online lub za pomocą aplikacji mobilnej',
		panelShipping: 'Nadajesz przesyłkę online lub za pomocą aplikacji mobilnej'
	},

	integrations: {
		pluginsEcommerceTitle: 'Pluginy <br> e-commerce',
		pluginsEcommerceDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi.',
		pluginsWebTitle: 'Plugin <br> web',
		pluginsWebDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi.',
		pluginsApiTitle: 'API',
		pluginsApiDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi.'
	},

	beOurDriver: {
		title: 'Zostań naszym <br> kierowcą',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi. Ut blandit vestibulum orci.',
		sendUsYourRequest: 'Wyślij swoje zgłoszenie <small>poprzez formularz poniżej lub skontaktuj się telefonicznie na <a class="link" href="tel:+48782402184">782 402 184</a></small>',
		monitorTitle: 'Jakaś ikona + treść o korzyściach',
		connectorTitle: 'Jakaś ikona + treść o korzyściach',
		apiTitle: 'Jakaś ikona + treść o korzyściach',
		terms: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi, id, ut. Dolorem reprehenderit quasi iste ex doloremque eaque commodi veritatis officiis quas nemo assumenda iusto, ipsa fugit labore explicabo delectus!',
		driverTypes: [
  		{ id: '1', name: 'Pieszo', icon: 'onfoot-active', value: 'onFoot' },
  		{ id: '2', name: 'Rower', icon: 'bike-active', value: 'bike' },
  		{ id: '3', name: 'Samochód', icon: 'car-active', value: 'car' },
  		{ id: '4', name: 'Bus', icon: 'bus-active', value: 'bus' },
  		{ id: '5', name: 'Ciężarówka', icon: 'truck-active', value: 'truck' }
  	]
	},

	newsletter: {
		title: 'Bądź z nami <br> na bieżąco',
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi. Ut blandit vestibulum orci nec.',
		subtitle: 'Zapisz się do newslettera',
		emailLabel: 'Twój adres email',
		termsNote: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris feugiat justo, id mollis arcu nunc vel tellus. Sed felis tortor, eleifend id nisl at, viverra rutrum nibh. Morbi non blandit mi.',
		submit: 'Zapisz się'
	},

	socialList: {
		headline: 'Znajdź nas na:',
		menu: [
			{ id: '0', name: 'facebook', url: '#', icon: 'facebook-icon' },
			{ id: '1', name: 'instagram', url: '#', icon: 'instagram-icon' },
			{ id: '2', name: 'twitter', url: '#', icon: 'twitter-icon' }
		]
	},

	footer: {
		description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque mattis, elit sed condimentum dapibus, urna mauris.',
		menu: [
			{ id: '0', name: 'FAQ', url: '/faq' },
			{ id: '1', name: 'O firmie', url: '/o-firmie' },
			{ id: '2', name: 'Regulamin', url: '/regulamin' },
			{ id: '3', name: 'Kontakt', url: '/kontakt' }
		]
	},

	// Subpages
	faq: {
		hero: {
			title: 'Witaj, <br>jak możemy Ci pomóc?',
			subtitle: 'Jesteśmy tu, aby pomagać.',
			backgroundImagePath: 'faq-top-bg.png'
		},
		title: 'Często zadawane pytania',
		mobileTitle: 'Aplikacja mobilna',
    mobile: [
      { id: '0', title: 'How Does The Free Trial Work?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '1', title: 'How Does The Free Trial Work?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '2', title: 'How Can I Pay For Delivery?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '3', title: 'Do I need to pay VAT?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '4', title: 'What Happens When I Cancel A Paid Account?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' }
    ],
    webTitle: 'Aplikacja webowa',
    web: [
      { id: '5', title: 'How Does The Free Trial Work?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '6', title: 'How Does The Free Trial Work?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '7', title: 'How Can I Pay For Delivery?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '8', title: 'Do I need to pay VAT?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '9', title: 'What Happens When I Cancel A Paid Account?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' }
    ],
    pluginTitle: 'Wtyczka internetowa',
    plugin: [
      { id: '10', title: 'How Does The Free Trial Work?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '11', title: 'How Does The Free Trial Work?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '12', title: 'How Can I Pay For Delivery?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '13', title: 'Do I need to pay VAT?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' },
      { id: '14', title: 'What Happens When I Cancel A Paid Account?', description: 'It’s completely free, no credit card required. Just register with your e-mail address and you can test SIimpliCIty with all it’s features available.' }
    ]
  },

  contact: {
		hero: {
			title: 'Masz do nas <br> jakieś pytanie?',
			subtitle: 'Skontaktuj się z naszym biurem obsługi',
			backgroundImagePath: 'contact-top-bg.png'
		},
		title: 'Biuro obsługi klienta',
		contactFormTitle: 'Formularz kontaktowy',
		contactFormDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
		techHelpTitle: 'Pomoc techniczna',
		techHelpDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam semper laoreet dolor sit amet aliquam. Aliquam erat volutpat. Proin fermentum sit amet lectus vel pellentesque. Integer elementum ligula nulla, sed ultrices lectus imperdiet eu. Etiam placerat justo risus, a iaculis risus hendrerit ac. Donec quam purus, posuere vestibulum dolor at, feugiat commodo dui.',
		termsNote: 'Lreet dolor sit amet aliquam. Aliquam erat volutpat.',
		contactFormMessage: 'Treść pytania',
		techFormIssue: 'Wybierz rodzaj problemu',
		techFormDescription: 'Opis',
		issues: [
			{ id: '0', name: 'Issue 1' },
			{ id: '1', name: 'Issue 2' },
			{ id: '2', name: 'Issue 3' },
			{ id: '3', name: 'Issue 4' },
			{ id: '4', name: 'Issue 5' }
		]
  }
})

export const mutations = {}

export const actions = {}

export const getters = {
	head (state) {
		return state.head
	},
	companyName (state) {
		return state.companyName
	},
	headlineH1 (state) {
		return state.headlineH1
	},
	ourSuppliersTitle (state) {
		return state.ourSuppliersTitle
	},
	ourSuppliers (state) {
		return state.ourSuppliers
	},
	loginBtn (state) {
		return state.loginBtn
	},
	registerBtn (state) {
		return state.registerBtn
	},
	toggleMenu (state) {
		return state.toggleMenu
	},
	closeMenu (state) {
		return state.closeMenu
	},
	read (state) {
		return state.read
	},
	download (state) {
		return state.download
	},
	downloadDocumentation (state) {
		return state.downloadDocumentation
	},
	form (state) {
		return state.form
	},
	login (state) {
		return state.login
	},
	register (state) {
		return state.register
	},
	downloadApp (state) {
		return state.downloadApp
	},
	integrations (state) {
		return state.integrations
	},
	howItWorks (state) {
		return state.howItWorks
	},
	beOurDriver (state) {
		return state.beOurDriver
	},
	newsletter (state) {
		return state.newsletter
	},
	socialList (state) {
		return state.socialList
	},
	footer (state) {
		return state.footer
	},
	// Subpages
	faq (state) {
		return state.faq
	},
	contact (state) {
		return state.contact
	}
}

export default { namespaced: true, state, mutations, actions, getters }
