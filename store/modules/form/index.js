const state = () => ({})

const mutations = {}

const actions = {
	submitRegisterForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	},

	submitLoginForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	},

	submitDeliveryForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	},

	submitDriverForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	},

	submitContactForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	},

	submitTechHelpForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	},
	
	submitNewsletterForm ({commit}, payload) {
		if (!payload) return
		console.log(payload)
	}
}

const getters = {}

export default { namespaced: true, state, mutations, actions, getters }
