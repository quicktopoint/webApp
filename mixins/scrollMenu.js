import { mapActions, mapGetters } from 'vuex'

export const scrollMenu = {
	methods: {
  	...mapActions('scrollMenu', [ 'changeActiveMenuItem', 'changeActiveMenuItemToNull' ]),
		changeRoute (hash) {
			if (this.$route.path !== '/')
				this.$router.push({ path: `/${hash}` })
		}
	},
	computed: {
		...mapGetters('scrollMenu', [ 'menu', 'activeMenuItem' ])
	}
}
