export const collapseToggler = {
  data () {
    return {
      collapseTogglerActive: null
    }
  },
  methods: {
    addCollapseTogglerActive (id) {
      if (this.collapseTogglerActive === id)
        this.collapseTogglerActive = null
      else
        this.collapseTogglerActive = id
    }
  }
}
