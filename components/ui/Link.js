import { mergeData } from 'vue-functional-data-merge'
import '@/assets/styles/components/_link.sass'

export const props = {
  active: {
    type: Boolean,
    default: false
  },
  tag: {
    type: String,
    default: 'a'
  },
  href: {
    type: String,
    default: null
  },
  to: {
    type: [String, Object],
    default: null
  },
  disabled: {
    type: Boolean,
    default: false
  }
}

export function computeTag (props, parent) {
  return Boolean(parent.$router) && props.to && !props.disabled ? 'nuxt-link' : 'a'
}

export function computeHref ({ disabled, href, to }, tag) {
  // We've already checked the parent.$router in computeTag,
  // so nuxt-link means live router.
  // When deferring to Vue Router's nuxt-link,
  // don't use the href attr at all.
  // Must return undefined for nuxt-link to populate href.
  if (tag === 'nuxt-link') return void 0
  // If href explicitly provided
  if (href) return href
  // Reconstruct href when `to` used, but no router
  if (to) {
    // Fallback to `to` prop (if `to` is a string)
    if (typeof to === 'string') return to
    // Fallback to `to.path` prop (if `to` is an object)
    if (typeof to === 'object' && typeof to.path === 'string') return to.path
  }
  // If nothing is provided use '#'
  return '#'
}

export function clickHandlerFactory ({ disabled, tag, href, suppliedHandler, parent }) {
  const isRouterLink = tag === 'nuxt-link'

  return function onClick (e) {
    if (disabled && e instanceof Event) {
      e.preventDefault()
      // Stop event from bubbling up.
      e.stopPropagation()
      // Kill the event loop attached to this specific EventTarget.
      e.stopImmediatePropagation()
    } else {
      parent.$root.$emit('clicked::link', e)

      if (isRouterLink && e.target.__vue__) 
        e.target.__vue__.$emit('click', e)

      if (typeof suppliedHandler === 'function') 
        suppliedHandler(...arguments)
    }

    if ((!isRouterLink && href === '#') || disabled)
      // Stop scroll-to-top behavior or navigation.
      e.preventDefault()
  }
}

export default {
  functional: true,
  props,
  render (h, { props, data, parent, children }) {
    const tag = computeTag(props, parent)
    const href = computeHref(props, tag)
    const eventType = tag === 'nuxt-link' ? 'nativeOn' : 'on'
    const suppliedHandler = (data[eventType] || {}).click
    const handlers = { click: clickHandlerFactory({ tag, href, disabled: props.disabled, suppliedHandler, parent }) }
    const componentData = mergeData(data, {
      staticClass: 'link',
      class: [
        props.active ? 'link--active' : null,
        props.disabled ? 'link--disabled' : null
      ],
      attrs: { href, disabled: props.disabled || null },
      props: { ...props, tag: props.routerTag }
    })

    // If href prop exists on nuxt-link (even undefined or null) it fails working on SSR
    if (!componentData.attrs.href)
      delete componentData.attrs.href

    // We want to overwrite any click handler since our callback
    // will invoke the supplied handler if !props.disabled
    componentData[eventType] = { ...componentData[eventType] || {}, ...handlers }
    
    return h(tag, componentData, children)
  }
}
