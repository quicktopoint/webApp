import { mergeData } from 'vue-functional-data-merge'
import Link, { props as linkProps } from '../Link'
import '@/assets/styles/components/_button.sass'

const size = ['sm', 'md', 'lg']
const icons = ['send', 'download', 'check']
const types = ['default', 'primary', 'success', 'info', 'warning', 'danger']
const nativeTypes = ['button', 'submit', 'reset']
const orientations = ['left', 'right']

const btnProps = {
  block: {
    type: Boolean,
    default: false
  },
  disabled: {
    type: Boolean,
    default: false
  },
  btnLink: {
    type: Boolean,
    default: false
  },
  size: {
    type: String,
    default: 'md',
    validator: value => size.includes(value)
  },
  type: {
    type: String,
    default: 'default',
    validator: value => types.includes(value)
  },
  nativeType: {
    type: String,
    default: null,
    validator: value => nativeTypes.includes(value)
  },
  tag: {
    type: String,
    default: 'button'
  },
  orientation: {
    type: String,
    default: 'right',
    validator: value => orientations.includes(value)
  }
}

export default {
  functional: true,
  props: { ...btnProps, ...linkProps },
  render (h, { props, data, listeners, children }) {
    const isLink = Boolean(props.href || props.to)
    const on = {
      click (e) {
        if (props.disabled && e instanceof Event) {
          e.stopPropagation()
          e.preventDefault()
          e.stopImmediatePropagation()
        }
      }
    }
    const componentData = mergeData(data, {
      staticClass: 'btn',
      class: [
        `btn--${props.type}`,
        `btn--${props.size}`,
        `btn--${props.orientation}`,
        props.btnLink ? 'btn--link' : null,
        props.block ? 'btn--block' : null,
        props.active ? 'btn--active' : null,
        props.disabled ? 'btn--disabled' : null,
      ],
      attrs: {
        href: isLink ? props.href : null,
        to: isLink ? props.to : null,
        disabled: props.disabled ? 'disabled' : null,
        type: props.nativeType
      },
      on
    })

    return h(isLink ? Link : 'button', componentData, children)
  }
}
