import { mergeData } from 'vue-functional-data-merge'
import '@/assets/styles/components/_panel.sass'

export const props = {
  tag: {
    type: String,
    default: 'div'
  },
  active: {
    type: Boolean,
    default: false
  },
  hasBackground: {
    type: Boolean,
    default: false
  },
  hasShadow: {
    type: Boolean,
    default: false
  }
}

export default {
  functional: true,
  props,
  render (h, { props, data, listeners, children }) {
    const componentData = {
      staticClass: 'panel',
      class: [
        props.active ? 'panel--active' : null,
        props.hasBackground ? 'panel--has-bg' : null,
        props.hasShadow ? 'panel--has-shadow' : null
      ]
    }

    return h(props.tag, mergeData(data, componentData), children)
  }
}
