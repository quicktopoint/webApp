import { mergeData } from 'vue-functional-data-merge'
import '@/assets/styles/components/grid/_row.sass'

const props = {
  tag: {
    type: String,
    default: 'div'
  },
  margin: {
    type: Boolean,
    default: false
  }
}

export default {
  name: 'Row',
  functional: true,
  props,
  render (h, { props, data, children }) {
    const componentData = {
      staticClass: 'row',
      class: { 'row--margin' : props.margin }
    }
    
    return h(props.tag, mergeData(data, componentData), children)
  }
}
