import { mergeData } from 'vue-functional-data-merge'
import '@/assets/styles/components/grid/_container.sass'

const props = {
  tag: {
    type: String,
    default: 'div'
  },
  fluid: {
    type: Boolean,
    default: false
  }
}

export default {
  name: 'Conatiner',
  functional: true,
  props,
  render (h, { props, data, children }) {
    const componentData = {
      staticClass: 'container',
      class: { 'container--fluid': props.fluid }
    }
    
    return h(props.tag, mergeData(data, componentData), children)
  }
}
