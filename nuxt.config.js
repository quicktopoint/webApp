module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'SimpliCity Connect',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=5, user-scalable=yes' },
      { hid: 'description', name: 'description', content: 'An app that allows you to deliver shipments in less than a day.' },
      { name: 'apple-mobile-web-app-title', content: 'SimpliCity Connect' },
      { name: 'application-name', content: 'SimpliCity Connect' },
      { name: 'msapplication-TileColor', content: '#1f3555' },
      { name: 'theme-color', content: '#1f3555' }
    ],
    link: [
      { rel: 'manifest', href: '/manifest.json' },
      { rel: 'shortcut icon', type: 'image/x-icon', sizes: '16x16 24x24 32x32 48x48 64x64 96x96', href: 'favicons/favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: 'favicons/apple-touch-icon.png' },
      { rel: 'mask-icon', href: 'favicons/safari-pinned-tab.svg', color: '#1f3555' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Jura:400,700|Work+Sans:400,500&amp;subset=latin-ext' }
    ]
  },
  /*
  ** Styles configuration
  */
  modules: [
    ['nuxt-sass-resources-loader', '@/assets/styles/main.sass']
  ],
  /*
  ** Router scrollBehavior
  */
  router: {
    scrollBehavior: function (to, from, savedPosition) {
      return { x: 0, y: 0 }
    }
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#04e68e' },
  /*
  ** Build configuration
  */
  build: {
    babel: {
      presets: [ 'es2015', 'stage-2' ],
      plugins: [ 'transform-runtime', 'transform-object-rest-spread', 'transform-async-to-generator' ]
    },
    vendor: [ 'axios', 'vuelidate', 'vue-lazyload', 'vue2-collapse' ],
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      if (!isClient) {
        // This instructs Webpack to include `vue2-google-maps`'s Vue files
        // for server-side rendering
        config.externals.splice(0, 0, function (context, request, callback) {
          if (/^vue2-google-maps($|\/)/.test(request)) {
            callback(null, false)
          } else {
            callback()
          }
        })
      }
    }
  },
  /*
  ** Plugins configuration
  */
  plugins: [
    // Global components
    { src: '~/plugins/global', ssr: true },
    // Real plugins
    { src: '~/plugins/vue-scrollto', ssr: false },
    { src: '~/plugins/vue-lazyload', ssr: false },
    { src: '~/plugins/vue-google-maps', ssr: false },
    { src: '~/plugins/vuelidate', ssr: false },
    { src: '~/plugins/vue-collapse', ssr: false }
  ]
}
