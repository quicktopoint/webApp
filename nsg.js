/*
* This simple script is our SPRITE CSS Generator
*
* For more info go there: https://www.npmjs.com/package/node-sprite-generator
*/
const nsg = require('node-sprite-generator')
const replace = require('replace')

nsg({
    src: [ 'static/icons/**/*.png', 'static/icons/**/*.jpg' ],
    spritePath: 'assets/images/sprite.png',
    stylesheetPath: 'assets/styles/components/_sprite.sass',
    stylesheet: 'css',
    compositor: 'jimp',
    layout: 'packed'
}, err => {
	if (!err) {
        replace({
            regex: '../../images/sprite.png',
            replacement: '~assets/images/sprite.png',
            paths: [ 'assets/styles/components/_sprite.sass' ],
            recursive: true,
            silent: true
        })
		console.log('Success')
    } else {
		console.log(err)	
    }
})
