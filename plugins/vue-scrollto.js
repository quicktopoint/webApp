/*
 * For more info go there: https://github.com/rigor789/vue-scrollto
 */
import Vue from 'vue'
import VueScrollTo from 'vue-scrollto'

const options = {
	duration: 750,
	easing: 'ease',
	offset: 0,
	cancelable: true,
	onStart: function (el) {
		let hash = '#' + el.getAttribute('id')
		if ($nuxt.$route.hash !== hash)
			$nuxt.$router.push({ path: hash })
	},
	onDone: false,
	onCancel: false,
	x: false,
	y: true
}

Vue.use(VueScrollTo, options)
