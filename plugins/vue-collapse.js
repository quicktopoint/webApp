/*
 * For more info go there: https://roszpun.github.io/vue-collapse/
 */
import Vue from 'vue'
import VueCollapse from 'vue2-collapse'

Vue.use(VueCollapse)
