/*
 * For more info go there: https://github.com/monterail/vuelidate
 */
import Vue from 'vue'
import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)
