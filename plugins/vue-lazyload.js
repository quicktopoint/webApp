/*
 * For more info go there: https://github.com/hilongjw/vue-lazyload
 */
import Vue from 'vue'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload)
