import Vue from 'vue'
import Container from '~/components/ui/grid/Container'
import Row from '~/components/ui/grid/Row'
import Column from '~/components/ui/grid/Column'
import Panel from '~/components/ui/Panel'
import List from '~/components/ui/list/List'
import ListItem from '~/components/ui/list/ListItem'
import Icon from '~/components/ui/Icon'
import Link from '~/components/ui/Link'
import Button from '~/components/ui/button/Button'
import ButtonIcon from '~/components/ui/button/ButtonIcon'

Vue.component('v-container', Container)
Vue.component('v-row', Row)
Vue.component('v-col', Column)
Vue.component('v-panel', Panel)
Vue.component('v-list', List)
Vue.component('v-list-item', ListItem)
Vue.component('v-icon', Icon)
Vue.component('v-link', Link)
Vue.component('v-btn', Button)
Vue.component('v-btn-icon', ButtonIcon)
